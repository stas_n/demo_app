# **demo_app** #

Sample of simple app

For deploy on local machine use following commands:

1. git clone:https://github.com/stasyanx/demo_app  - Clone project on your local machine

2. bundle install   - Install gems

3. rake db:setup -  Setup DB

## Working  example ##
[Demo](fast-demo-app.herokuapp.com)